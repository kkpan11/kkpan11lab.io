---
title: "About"
Description: "Making the Complex Simple and Easy to Understand!"
layout: "about"
---

![](https://www.gravatar.com/avatar/a4e3dafd95983ca2d4f2538d7ad816a8)

Hi, I'm Gianni.

---
Software developer with five years' experience in implementing a variety of innovative products on the mobile, wearable and medical startups. Also worked on a large-scale global e-commerce company for two and half years with global teams.
I’m looking forward to bringing together professionals from various fields to solve difficult problems in the world.
Hope I can do something for human beings on the earth. As a coder, book lover, enthusiastic cyclist, type 1 diabetic.

---

- [Social Links](https://about.me/kkpan11)