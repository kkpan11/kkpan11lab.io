---
headless: true
---

- [**About**]({{< relref "about.md" >}})
---
- [**Projects**]({{< relref "/projects/" >}})
  - [Rakuma _(2020-2021)_]({{< relref "/projects/Rakuma/" >}})
  - [Rakuten Ichiba _(2018-2020)_]({{< relref "/projects/Taiwan Ichiba/" >}})
  - [Smart BP _(2016-2017)_]({{< relref "/projects/Smart BP/" >}})
  - [Pulse _(2015)_]({{< relref "/projects/Pulse/" >}})
  - [TallyUp for Costco _(2014)_]({{< relref "/projects/TallyUp for Costco/" >}})
  - [Life of Pee _(2013)_]({{< relref "/projects/Life of Pee/" >}})
  - [Valen Hsu _(2012)_]({{< relref "/projects/Valen Hsu/" >}})