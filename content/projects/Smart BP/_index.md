---
title: Smart BP
date: 2020-06-01
subtitle: 2013
---

# Smart BP #

- **MIT Technology Review:** [An Easier Way to Track Your Blood Pressure](https://www.technologyreview.com/2016/04/15/160968/an-easier-way-to-track-your-blood-pressure/)
- **TechCrunch:** [The best of Highway1’s hardware accelerator demo day](https://techcrunch.com/2016/05/18/highway1-may-2016/)

{{< video src="/videos/SmartBP-Cut.mp4" type="video/mp4" preload="auto" >}}