---
title: Taiwan Ichiba
date: 2020-06-01
subtitle: 2013
---

# Taiwan Ichiba #

Global Ichiba's service is closed, so only provided Taiwan's service reference.

PS. Ichiba(市場) is a Japanese word which means "market".

- [Rakuten Taiwan Ichiba](https://www.rakuten.com.tw/app/)
    - [App Store Link](https://apps.apple.com/app/id762167763)